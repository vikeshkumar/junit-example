package net.vikesh.training;

/**
 * The modern fibonacci series starts from 0
 * 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946, 17711, 28657, 46368, 75025, 121393, 196418, 317811
 */
public class Fibonacci {
    public long nthFibonacci(final int n) {
        if (n <= 0) {
            throw new IllegalArgumentException("0 is invalid position");
        }
//        //termination
        if (n == 1) {
            return 0L;
        } else if (n <= 2) {
            return 1L;
        }
        else {
            return nthFibonacci(n - 1)+nthFibonacci(n-2);
        }
    }
}
