package net.vikesh.training;

import org.junit.Assert;
import org.junit.Test;

public class FibonacciTest {
    private Fibonacci fibonacci = new Fibonacci();

    @Test
    public void nthFibonacciShouldReturnZero() {
        long expected = 0L;
        long actual = fibonacci.nthFibonacci(1);
        Assert.assertEquals("Value was not 0", expected, actual);
    }


    @Test
    public void nthFibonacciShouldReturnFive() {
        long expected = 5L;
        long actual = fibonacci.nthFibonacci(6);
        Assert.assertEquals("Value was not 0", expected, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nthFibnonacciShouldThrowError() {
        fibonacci.nthFibonacci(0);
    }
}
